//Inicio
import express from 'express';
import { fileURLToPath } from 'url';
import path from 'path';

import myRoutes from './router/index.js';

const puerto = 80; 
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const main = express();


main.set('view engine', 'ejs');


main.use(express.static(path.join(__dirname, 'public')));

main.use(express.urlencoded({ extended: true }));

main.use(myRoutes.router);

main.listen(puerto, () => {
  console.log(`Servidor corriendo en puerto:${puerto}`);
});